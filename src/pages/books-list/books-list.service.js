import AppService from '../../app.service';
export default class BookListService {

    constructor() {
        this.appSrv = new AppService();
    }

    getBooks() {
        return new Promise(resolve => {
            this.appSrv.getBooksFromJson()
                .then(booksArray => resolve(booksArray));
        })
    }
    getGenres() {
        return new Promise(resolve => {
            this.appSrv.getGenresFromJson()
                .then(genresArray => resolve(genresArray));
        })

    }

    deleteBook(bookItem) {
        let auxArray = JSON.parse(localStorage.getItem('booksArray'));
        auxArray.forEach((book, ind) => {
            if (book.id === bookItem.id) {
                auxArray.splice(ind, 1);
            }
        })
        localStorage.setItem('booksArray', JSON.stringify(auxArray));
        return auxArray;
    }

    setBookToUpdate(book) {
        localStorage.setItem('book', JSON.stringify(book));
    }

}