import React, { Component } from 'react';
import './books-list.scss';
import BooksListService from './books-list.service';
import Book from '../../components/book/book';
import Reader from '../../components/reader/reader';
import { Glyphicon } from 'react-bootstrap';

export default class BooksFormPage extends Component {
    constructor() {
        super();
        this.state = {
            books: [],
            query: '',
            genres: [],
            showGenres: false,
            section: 'All',
            bookToShow: ''
        }
        this.initialArray = [];
        this.booksSrv = new BooksListService();

    }

    componentWillMount() {
        this.booksSrv.getBooks()
            .then(booksArray => {
                this.initialArray = booksArray;
                this.setState({ books: booksArray })
            });
        this.booksSrv.getGenres()
            .then(genresArray => this.setState({ genres: genresArray }));
    }

    deleteBook(book) {
        this.setState({ bookToShow: '', books: this.booksSrv.deleteBook(book) });
    }
    editBook(book) {
        this.booksSrv.setBookToUpdate(book);
        this.props.router.push('/book-form');
    }

    filter(genre) {
        let auxArray = this.initialArray.filter((book, ind) => {
            return book.genre === genre;
        })
        this.setState({ section: genre, books: auxArray, showGenres: false });
    }
    showFilter() {
        this.setState({ showGenres: !this.state.showGenres })
    }

    restoreData() {
        this.setState({ showGenres: false, section: 'All', books: this.initialArray });
    }


    showDetails(book) {
        this.setState({ bookToShow: book });
    }
    close() {
        this.setState({ bookToShow: '' });

    }


    render() {
        return (
            <div className="container">
                <div className="filter-container">
                    <Glyphicon className="filter" onClick={() => this.showFilter()} glyph="glyphicon glyphicon-filter"></Glyphicon>
                    {this.state.showGenres === true ?
                        <div className="filter-display">
                            <span className="genre" onClick={() => this.restoreData()}>All</span>
                            {this.state.genres.map((genre, key) => {
                                return (
                                    <span className="genre" key={key} onClick={() => this.filter(genre.genre)}>{genre.genre}</span>
                                )
                            })}

                        </div>
                        :
                        <div></div>
                    }
                </div>
                <div className="section">{this.state.section}</div>
                <div className="carousel">
                    {this.state.books.map((book, ind) => {
                        return (
                            <Book
                                key={ind}
                                delete={() => { this.deleteBook(book) }}
                                edit={() => { this.editBook(book) }}
                                details={() => { this.showDetails(book) }}
                                book={book} />
                        )
                    })}
                </div>
                {this.state.bookToShow ?
                    <Reader bookToShow={this.state.bookToShow} close={() => this.close()} />
                    : <div></div>
                }
            </div>
        );
    }
}

