import React, { Component } from 'react';
import { Col, Form, Button, ControlLabel, FormGroup, DropdownButton, MenuItem, FormControl } from 'react-bootstrap';
import './books-form.scss';
import BooksFormService from './books-form.service';


export default class FormPage extends Component {
    constructor() {
        super();
        this.formSrv = new BooksFormService();
        this.state = {
            id: this.getUniqueID(),
            title: '',
            author: '',
            description: '',
            genre: '',
            price: '',
            img: '/img/covers/no-img.jpg',
            genres: []
        }
        // 0 => add 1 => edit
        this.mode = 0;
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
        this.formSrv.getGenres()
            .then(genresArray => {
                this.setState({ genres: genresArray });
            });
        let book = this.formSrv.getBookToUpdate();
        if (book !== null) {
            this.setState({
                id: book.id,
                title: book.title,
                author: book.author,
                description: book.description,
                genre: book.genre,
                price: book.price,
                img: book.img,
            })
            this.mode = 1;
        }
    }
    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    };
    getUniqueID() {
        return new Date().getTime + Math.random() * 1000;
    }

    submit() {
        delete this.state.genres;
        this.formSrv.saveBook(this.state);
        this.props.router.push('/');
    }

    updateBook() {
        delete this.state.genres;
        this.formSrv.updateBook(this.state);
        this.props.router.push('/');

    }

    cancel() {
        this.formSrv.deleteBookToUpdate();
        this.props.router.push('/');

    }

    render() {
        return (
            <div className="form-container">
                <Form horizontal>
                    <FormGroup controlId="formHorizontalEmail">
                        <Col componentClass={ControlLabel} sm={2}>Title</Col>
                        <Col sm={6}>
                            <FormControl
                                className="input"
                                type="text"
                                name="title"
                                placeholder="Enter a title..."
                                value={this.state.title}
                                onChange={this.handleChange}
                                onKeyPress={event => {
                                    if (event.key === 'Enter') {
                                        this.submit();
                                    }
                                }}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalPassword">
                        <Col componentClass={ControlLabel} sm={2}>Description </Col>
                        <Col sm={6}>
                            <FormControl
                                className="textarea"
                                name="description"
                                type="text"
                                componentClass="textarea"
                                placeholder="Enter a llitle description"
                                value={this.state.description}
                                onChange={this.handleChange}
                                onKeyPress={event => {
                                    if (event.key === 'Enter') {
                                        this.submit();
                                    }
                                }}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup controlId="formHorizontalPassword">
                        <Col componentClass={ControlLabel} sm={2}>Price</Col>
                        <Col sm={6}>
                            <FormControl
                                className="input"
                                type="number"
                                name="price"
                                placeholder="set a price"
                                value={this.state.price}
                                onChange={this.handleChange}
                                onKeyPress={event => {
                                    if (event.key === 'Enter') {
                                        this.submit();
                                    }
                                }}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup controlId="formHorizontalPassword">
                        <Col componentClass={ControlLabel} sm={2}>Author</Col>
                        <Col sm={6}>
                            <FormControl
                                className="input"
                                name="author"
                                type="text"
                                placeholder="Enter an author"
                                value={this.state.author}
                                onChange={this.handleChange}

                            />
                        </Col>
                    </FormGroup>

                    <FormGroup>
                        <Col componentClass={ControlLabel} sm={2}>Genre </Col>
                        <Col sm={2}>

                            <DropdownButton title="genres" className="input" id="drop">
                                {
                                    this.state.genres.map((genre, ind) => {
                                        return (
                                            <MenuItem eventKey={genre.genre} key={genre.id} value={this.state.genre}
                                                onSelect={event => this.setState({ genre: event })}>{genre.genre}</MenuItem>
                                        )
                                    })
                                }
                            </DropdownButton>
                            <span className="display-genre">  {this.state.genre}</span>

                        </Col>
                        <Col sm={4} className="text-right">
                            {this.mode === 0 ? <Button bsStyle="success" onClick={() => this.submit()}>Submit</Button>
                                : <Button bsStyle="success" onClick={() => this.updateBook()}>Update</Button>
                            }
                            <Button bsStyle="danger" onClick={() => this.cancel()}>Cancel</Button>
                        </Col>
                    </FormGroup>
                    <FormGroup>


                    </FormGroup>
                </Form>
            </div>
        )
    }
}