import AppService from '../../app.service';

export default class BooksFormService {

    constructor() {
        this.appSrv = new AppService();
        this.appSrv.getGenresFromJson();
    }

    getGenres() {
        return new Promise(resolve => {
            this.appSrv.getGenresFromJson()
                .then(genresArray => resolve(genresArray));
        })

    }
    saveBook(book) {
        let auxArray = this.getBooks();
        auxArray.unshift(book);
        this.setBooks(auxArray);
    }
    getBookToUpdate() {
        return JSON.parse(localStorage.getItem('book'));
    }

    getBooks() {
        return JSON.parse(localStorage.getItem('booksArray'));
    }
    deleteBooks() {
        localStorage.removeItem('booksArray');
    }
    setBooks(booksArray) {
        this.deleteBooks();
        localStorage.setItem('booksArray', JSON.stringify(booksArray));
    }
    updateBook(bookItem) {
        let auxArray = this.getBooks();
        auxArray.forEach((book, ind) => {
            if (book.id === bookItem.id) {
                auxArray[ind] = bookItem;
            }
        });
        this.setBooks(auxArray);
        this.deleteBookToUpdate();
    }
    deleteBookToUpdate() {
        localStorage.removeItem('book');
    }
}