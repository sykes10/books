import AppService from '../../app.service';

export default class GenresFormService {

    constructor() {
        this.appSrv = new AppService();
        this.appSrv.getGenresFromJson();
    }
    getInitialGenres() {
        return new Promise(resolve => {
            this.appSrv.getGenresFromJson()
                .then(genresArray => resolve(genresArray));
        })
    }
    saveGenre(genre) {
        let auxArray = this.getGenres();
        auxArray.unshift(genre);
        this.setGenres(auxArray);
    }
    getGenres() {
        return JSON.parse(localStorage.getItem('genresArray'));
    }
    deleteGenres() {
        localStorage.removeItem('genresArray');
    }
    deleteGenre(genreItem) {
        let auxArray = JSON.parse(localStorage.getItem('genresArray'));
        auxArray.forEach((genre, ind) => {
            if (genre.id === genreItem.id) {
                auxArray.splice(ind, 1);
            }
        })
        localStorage.setItem('genresArray', JSON.stringify(auxArray));
        return auxArray;
    }
    updateGenres(genreItem) {
        let auxArray = this.getGenres();
        auxArray.forEach((genre, ind) => {
            if (genre.id === genreItem.id) {
                auxArray[ind] = genreItem;
            }
        });
        this.setGenres(auxArray);
    }
    setGenres(genresArray) {
        this.deleteGenres();
        localStorage.setItem('genresArray', JSON.stringify(genresArray));
    }


}