import React, { Component } from 'react';
import './genres-form.scss';
import { Glyphicon, Form, FormGroup, FormControl } from 'react-bootstrap';

import GenresFormService from './genres-form.service';


export default class GenresFormPage extends Component {
    constructor() {
        super();
        this.formSrv = new GenresFormService();
        this.state = {
            genres: [],
            newGenre: {
                id: this.getUniqueID(),
                genre: ''
            },
            // mode => 0 - add / 1 - edit
            mode: 0,
            showInput: false
        }
        this.handleChange = this.handleChange.bind(this);

        this.initiaGenreArray = [];
    }
    getUniqueID() {
        return new Date().getTime + Math.random * 1000;
    }
    componentWillMount() {
        this.formSrv.getInitialGenres().then(genresArray => {
            this.initiaGenreArray = genresArray;
            this.setState({ genres: genresArray });
        })
    }

    addGenre() {
        this.setState({ showInput: false })
    }
    handleChange(event) {
        const {value, name} = event.target;
        this.setState({
            [name]: { genre: value, id: this.state.newGenre.id }
        });
    };
    handleSubmit() {
        if (this.state.mode === 0) {
            this.formSrv.saveGenre(this.state.newGenre)
            this.setState({ genres: this.formSrv.getGenres(), showInput: false });
        } else {
            this.formSrv.updateGenres(this.state.newGenre)
            this.setState({ genres: this.formSrv.getGenres(), showInput: false });
        }
    }

    editGenre(genre) {
        this.setState({ showInput: true, mode: 1, newGenre: { genre: genre.genre, id: genre.id } })
    }

    deleteGenre(genre) {
        this.setState({ showInput: false, genres: this.formSrv.deleteGenre(genre) });
    }
    cancel() {
        this.setState({ showInput: false });
    }

    render() {
        return (
            <div className="container">

                <div className="genre-list">
                    {this.state.showInput ?
                        <div>
                            <Form >
                                <FormGroup controlId="formHorizontalEmail">
                                    <FormControl
                                        className="input"
                                        type="text"
                                        name="newGenre"
                                        placeholder="Enter a genre"
                                        value={this.state.newGenre.genre}
                                        onChange={this.handleChange}
                                    />
                                </FormGroup>
                            </Form>
                            <Glyphicon onClick={() => this.handleSubmit()} className="save-button" glyph="glyphicon glyphicon-floppy-disk"></Glyphicon>
                            <Glyphicon onClick={() => this.cancel()} className="cancel-button" glyph="glyphicon glyphicon-remove"></Glyphicon>

                        </div>
                        : <button className="addGenre" onClick={() => { this.setState({ showInput: true }) }}>Add Genre</button>
                    }


                    {this.state.genres.map((genre, ind) => {
                        return (
                            <div className="genre" key={ind}>{genre.genre}
                                <Glyphicon
                                    className="delete"
                                    onClick={() => this.deleteGenre(genre)}
                                    glyph="glyphicon glyphicon-remove"></Glyphicon>
                                <Glyphicon
                                    className="edit"
                                    onClick={() => this.editGenre(genre)}
                                    glyph="glyphicon glyphicon-pencil"></Glyphicon>
                            </div>)
                    })}
                </div>
            </div >
        )
    }
}