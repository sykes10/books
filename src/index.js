import React from 'react';
import ReactDOM from 'react-dom';

// routes
import Routes from './routes';

import Navbar from './components/navbar/navbar';

ReactDOM.render(
    <div>
        <Navbar />
        <Routes />
    </div>,
    document.getElementById('container')
);