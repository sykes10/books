import React, { Component } from 'react';
import { browserHistory, Router, Route } from 'react-router';

// pages
import BooksListPage from './pages/books-list/books-list';
import BooksFormPage from './pages/books-form/books-form';
import GenresFormPage from './pages/genres-form/genres-form';

// styles
import './style.scss';

export default class Routes extends Component {
    render() {
        return (
            <Router history={browserHistory} >
                <Route path="/" component={BooksListPage} />
                <Route path="/book-form" component={BooksFormPage} />
                <Route path="/genres-form" component={GenresFormPage} />
            </Router >
        );
    }
}