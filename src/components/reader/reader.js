import React, { Component } from 'react';
import './reader.scss';
import { Glyphicon } from 'react-bootstrap';


export default class Reader extends Component {
    render() {
        return (
            <div className="reader" id="reader">
                <Glyphicon className="close-icon" onClick={() => this.props.close()} glyph="glyphicon glyphicon-remove"></Glyphicon>
                <div className="reader-container">
                    <img className="img-full" src={this.props.bookToShow.img} alt={this.props.bookToShow.title} />
                    <div className="info">
                        <span className="title">{this.props.bookToShow.title}</span>
                        <span className="author">{this.props.bookToShow.author}</span>
                        <span className="description">{this.props.bookToShow.description}</span>
                        <span className="genre">{this.props.bookToShow.genre}</span>
                        <span className="price">{this.props.bookToShow.price ? this.props.bookToShow.price + ' €' : ''}</span>
                    </div>
                </div>
            </div>
        )
    }
}