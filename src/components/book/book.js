import React, { Component } from 'react';
import { Glyphicon } from 'react-bootstrap';
import './book.scss';

export default class Book extends Component {

    render() {
        return (
            <div className="book-item" style={{ backgroundImage: `url(${this.props.book.img})` }}>
                <div className="task-menu">
                    <Glyphicon className="delete" onClick={() => this.props.delete()} glyph="glyphicon glyphicon-remove"></Glyphicon>
                    <Glyphicon className="edit" onClick={() => this.props.edit()} glyph="glyphicon glyphicon-pencil"></Glyphicon>
                    <Glyphicon className="fav" onClick={() => this.props.fav()} glyph="glyphicon glyphicon-star-empty"></Glyphicon>

                    <a href="#reader"><Glyphicon className="view-more" onClick={() => this.props.details()} glyph="glyphicon glyphicon-resize-full"></Glyphicon></a>
                </div>
                <div className="info">
                    <span className="title">{this.props.book.title}</span>
                    <span className="author">{this.props.book.author}</span>
                    <span className="description">{this.props.book.description}</span>
                    <span className="genre">{this.props.book.genre}</span>
                    <span className="price">{this.props.book.price ? this.props.book.price + ' €' : ''}</span>
                </div>
                <div className="overlay">
                </div>
            </div>
        )
    }
}