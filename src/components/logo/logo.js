import React, { Component } from 'react';
import './logo.scss';

export default class Logo extends Component {
    render() {
        return (
            <div className="logo">
                <a href="/"><span>Beezy</span>
                </a>
            </div>
        )
    }
}
