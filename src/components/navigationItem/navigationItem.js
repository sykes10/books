
import React, { Component } from 'react';
import './navigationItem.scss';

export default class navigationItem extends Component {
    render() {
        return (
            <div className="navigationItem">
                <a href={this.props.path}><span className="text">{this.props.text}</span></a>
            </div>
        )
    }
}
