import React, { Component } from 'react';
import './navbar.scss';
import Logo from '../logo/logo';
import NavigationItem from '../navigationItem/navigationItem';

export default class Navbar extends Component {
    render() {
        return (
            <div className="navbar">
                <Logo />
                <div className="navbarContainer">
                    <NavigationItem text="Home" path="/" />
                    <span className="divider">*</span>
                    <NavigationItem text="Genres" path="/genres-form" />
                    <span className="divider">*</span>
                    <NavigationItem text="New Book" path="/book-form" />
                </div>
            </div>
        )
    }
}
