export default class AppService {

    getBooksFromJson() {
        return new Promise(
            (resolve) => {
                const BOOKS_URL = 'json/booksArray.json';
                const CONFIG = {
                    method: 'GET'
                }
                let books = localStorage.getItem('booksArray');
                if (books !== null) {
                    resolve(JSON.parse(books));
                } else {
                    fetch(BOOKS_URL, CONFIG)
                        .then(response => response.json())
                        .then(data => {
                            localStorage.setItem('booksArray', JSON.stringify(data));
                            resolve(data);
                        })
                }
            })
    }
    getGenresFromJson() {
        return new Promise(
            (resolve) => {
                const BOOKS_URL = 'json/genresArray.json';
                const CONFIG = {
                    method: 'GET'
                }
                let genres = localStorage.getItem('genresArray');
                if (genres !== null) {
                    resolve(JSON.parse(genres));
                } else {
                    fetch(BOOKS_URL, CONFIG)
                        .then(response => response.json())
                        .then(data => {
                            localStorage.setItem('genresArray', JSON.stringify(data));
                            resolve(data);
                        })
                }
            })
    }

}

